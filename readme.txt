
# input device plugin
https://doc.qt.io/archives/qtextended4.4/tut-deviceexample.html

http://doc.qt.io/qt-4.8/qt-embedded-charinput.html
http://blog.csdn.net/myths_0/article/details/24431597


http://www.arm9board.net/wiki/index.php?title=Compiling_QT_using_Buildroot&oldid=559

# regulator express
http://www.cnblogs.com/bingcaihuang/archive/2011/04/03/2004210.html

# threads
http://blog.debao.me/2013/08/how-to-use-qthread-in-the-right-way-part-1/
http://blog.debao.me/2013/08/how-to-use-qthread-in-the-right-way-part-2/

#debug
https://forum.qt.io/topic/3887/qmake-legendary-scope-switch-config-debug-debug-release/2
http://doc.qt.io/qt-4.8/debug.html
http://woboq.com/blog/nice-debug-output-with-qt.html

background image
http://www.programdevelop.com/1057965/

# box model
http://doc.qt.io/qt-4.8/stylesheet-customizing.html

# stylesheet
http://doc.qt.io/qt-4.8/stylesheet-reference.html

# font
http://www.kuqin.com/qtdocument/simple-qfont-demo-walkthrough.html
# font point size v.s. pixel size
http://developers.evrsoft.com/articles/pixel_vs_point_size_fonts.shtml
http://kyleschaeffer.com/development/css-font-size-em-vs-px-vs-pt-vs/

# touch mouse event
http://www.ptrackapp.com/apclassys-notes/embedded-linux-multitouch/
http://doc.qt.io/qt-4.8/gestures-overview.html
http://blog.qt.io/blog/2006/05/27/mouse-event-propagation/
http://www.qtcentre.org/threads/38074-QEvents-subclassing-QListWidget-not-getting-mouse-press-or-release

# all
http://www.ptrackapp.com/apclassys-notes/

# event
http://www.programmershare.com/1609100/
https://lnguin.wordpress.com/2013/05/23/qapplication-capture-mouse-event/
http://doc.qt.io/qt-4.8/eventsandfilters.html

# model&view
http://doc.qt.io/qt-4.8/model-view-programming.html#model-view-classes
# delegate
http://www.2cto.com/kf/201412/362329.html
http://www.qtcentre.org/archive/index.php/t-27777.html
http://labs.trolltech.com/blogs/2007/11/01/qstyleditemdelegate-styling-item-views
http://www.qtcentre.org/threads/27777-Customize-QListWidgetItem-how-to

#
http://www.codeproject.com/Articles/484905/Use-QNetworkAccessManager-for-synchronous-download


# gestures
http://erxiaoqu.com/archiver/?tid-111.html
https://doc.qt.io/archives/qq/qq18-mousegestures-code.zip

# object tree
# parent-children relationship
http://doc.qt.io/qt-4.8/objecttrees.html
https://doc.qt.io/archives/qq/qq03-big-brother.html



# build qt
https://wiki.linaro.org/WorkingGroups/Middleware/Graphics/Qt/CrossCompiling#Prepare_cross_compile_toolchain
https://wiki.qt.io/RaspberryPi_Beginners_Guide

